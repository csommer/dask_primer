{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e8dd4e14-ae26-41db-9ffe-e2fb6762f834",
   "metadata": {},
   "source": [
    "# *Dask* primer for larger-than-memory image analysis\n",
    "\n",
    "Bonus: Parallel execution for faster processing\n",
    "\n",
    "**AIAI meeting 10/06/2024**\n",
    "\n",
    "**Christoph Sommer (ISTA)**\n",
    "\n",
    "\n",
    "\n",
    "---\n",
    "\n",
    "## Outline\n",
    "---\n",
    "\n",
    "#### 1. Why Dask?\n",
    "#### 2. Dask Arrays\n",
    "#### 3. Dask-Image\n",
    "#### 4. Example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39a95ec0-244d-4912-888f-854288c7fe73",
   "metadata": {},
   "source": [
    "## 1. Why [Dask](https://docs.dask.org/en/stable/)?\n",
    "---\n",
    "\n",
    "### Typical In memory workflow:\n",
    "\n",
    "```python\n",
    "img = load_data(\"image.tif\")\n",
    "res = process_data(img)\n",
    "save_results(res)\n",
    "```\n",
    "\n",
    "###  Large (multi-dimensional) images will lead to `OutOfMemoryError`\n",
    "\n",
    "* in `load_data()` when the image itself does not fit into main memory.\n",
    "* in `process_data()` when the result or intermediate results do not fit. Raw data is typically stored as (unsigned)-integer with 8 or 16-bit. Intermediate resutls require 32 or 64-bit.\n",
    "* even if `img` and `res` just fit to memory, memory fragmentation might cause issues.\n",
    "\n",
    "### Where do such big images come from?\n",
    "\n",
    "* Light-Sheets microscopes\n",
    "* Cryo EM/ET\n",
    "* Stitching of multi-tile / multi-position experiments\n",
    "\n",
    "### How to deal with it?\n",
    "\n",
    "1. **Get more RAM!** Does not harm, but it's not always possible, since memory slots are fixed and limited. \n",
    "2. **Compression?** No, that only saves space on disk.\n",
    "3. **Swapping to disk** Very slow...\n",
    "4. **Custom block-wise processing** Project specific and hard to generalize, needs custom parallelization, too!\n",
    "5. **Block-wise processing using Dask** (when using Python or other distributed computing tools such as [Spark](https://docs.dask.org/en/latest/spark.html) (for ☕ friends).\n",
    "\n",
    "### Prerequisites\n",
    "\n",
    "Block-wise processing requires *efficient block-wise loading* of data from disk. \n",
    "\n",
    "This is supported in *chunked* file or container formats, such as zarr, N5, HDF5, CZI, LIF, OME-NGFF, ...\n",
    "\n",
    "For reading many microscopy file formats in Python, [AICSImageIO](https://github.com/AllenCellModeling/aicsimageio) supports reading as Dask arrays.\n",
    "\n",
    "Writing results:\n",
    "\n",
    "* using Dask directly: zarr, hdf5, npy\n",
    "* using Aicsimageio: ome_tiff, ome_ngff \n",
    "\n",
    "### What is [Dask](https://docs.dask.org/en/stable/)?\n",
    "\n",
    "[Dask](https://docs.dask.org/en/stable/) is a Python library for parallel and distributed computing.\n",
    "\n",
    "* Easy to set up (it’s just a Python library)\n",
    "* Dask is written in Python and only really supports Python. \n",
    "* Dask (since 2014) is an extension to the NumPy/Pandas/Scikit-learn/Jupyter stack.\n",
    "* Dask scales from a single node to thousand-node clusters. (have not tried yet)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c3c838f-9623-4a8d-b67d-12cee44d36e2",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "8080135d-e587-4ae0-8c55-1bc913ee5830",
   "metadata": {},
   "source": [
    "## 2. Dask Arrays\n",
    "---\n",
    "\n",
    "* Dask arrays are a collection of numpy arrays (see below)\n",
    "* These numpy arrays are only computed or loaded into memory if explicitely asked to! This is called *Lazy evaluaton* implemented by a *[Task Graphs](https://docs.dask.org/en/latest/graphs.html)*\n",
    "  \n",
    "The Dask array API is similar (a subset) to the Numpy array API\n",
    "___\n",
    "\n",
    "![image.png](https://docs.dask.org/en/stable/_images/dask-array.svg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "905595fc-84ac-4ce4-b52f-bcee02b6c310",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "86e2a1d6-d795-4c3d-8cfb-73f3cb1a06e7",
   "metadata": {},
   "source": [
    "### 2.1 Dask primitives on synthetic data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a250a34-23a0-4ad4-a265-cb1bb1d1ec84",
   "metadata": {},
   "outputs": [],
   "source": [
    "import dask.array as da\n",
    "import numpy as np\n",
    "\n",
    "# example data\n",
    "\n",
    "x = da.random.random((10000, 10000))\n",
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6d7a4605-e5c2-4af2-82d7-393a68d9b46f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's standardize this array by substracting mean and dividing by the std\n",
    "\n",
    "x_center = x - x.mean(axis=0)\n",
    "z = x_center / x.std(axis=0)\n",
    "z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "939b2f31-76d1-43c7-a9d2-3265603eb971",
   "metadata": {},
   "outputs": [],
   "source": [
    "# The task graph\n",
    "\n",
    "z.visualize()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "62181a93-f50c-4a2b-85a7-74339f5c6b10",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Trigger execution, result must fit into memory!\n",
    "\n",
    "z.compute()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42f6cdf1-29aa-4738-a5c8-f5051ce736ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save results, result must fit on disk! :-)\n",
    "\n",
    "z.to_hdf5(\"zscore.h5\", datapath=\"data\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6257e2fd-a652-4f4e-991d-d9422ece9083",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "642c03a5-d553-42be-bf7d-08dd10c7599d",
   "metadata": {},
   "source": [
    "### 2.2 Combining dask with scikit-image\n",
    "Apply skimage equalize_adapthist (CLAHE) blockwise with Dask\n",
    "\n",
    "*adapted from Marvin Albert from [Memory efficient way to compute adaptive histogram normalisation (CLAHE)](https://forum.image.sc/t/memory-efficient-way-to-compute-adaptive-histogram-normalisation-clahe/83758/4)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "295c2dcc-67ae-4246-9693-83e33716ecd8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from scipy import ndimage as ndi\n",
    "from skimage.exposure import equalize_adapthist\n",
    "\n",
    "### Synthetic example image\n",
    "im = np.clip(ndi.zoom(np.random.random([10] * 2), 50, order=3), 0, 1)\n",
    "\n",
    "### CLAHE parameters\n",
    "kernel_size = (64, 64)\n",
    "clip_limit = 0.1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f7b00e0-2073-44a1-b257-098a9e876159",
   "metadata": {},
   "source": [
    "#### In-memory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fde3510c-d30d-4362-b906-bd1fcf952262",
   "metadata": {},
   "outputs": [],
   "source": [
    "clahe_in_mem = equalize_adapthist(im, kernel_size=kernel_size, clip_limit=clip_limit)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "062c1e0e-1118-47eb-bcac-c3b27f579fc0",
   "metadata": {},
   "source": [
    "#### Dask blockwise"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f80aecbb-2a13-4b31-b654-ab888e19481c",
   "metadata": {},
   "outputs": [],
   "source": [
    "### Dask\n",
    "im_da = da.from_array(im, chunks=(256, 256))\n",
    "\n",
    "## block-wise\n",
    "clahe_dask_blockwise = da.map_blocks(\n",
    "    equalize_adapthist,\n",
    "    im_da,\n",
    "    kernel_size=kernel_size,\n",
    "    clip_limit=clip_limit,\n",
    ").compute()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "274644f0-08a2-44fc-9977-975e5f59290d",
   "metadata": {},
   "source": [
    "#### Dask blockwise with overlap"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0217e2a9-72d4-46ca-aad6-85a75c72b6ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "## block-wise with overlap\n",
    "clahe_dask_blockwise_overlap = da.map_overlap(\n",
    "    equalize_adapthist,\n",
    "    im_da,\n",
    "    kernel_size=kernel_size,\n",
    "    clip_limit=clip_limit,\n",
    "    depth=(64, 64),\n",
    "    dtype=float,\n",
    ").compute()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82d64814-f7c5-4a42-9d07-9fbd87f62197",
   "metadata": {},
   "source": [
    "#### Resutls"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36e8af63-54a7-4a3b-88c9-66cf8c253d51",
   "metadata": {},
   "outputs": [],
   "source": [
    "f, axs = plt.subplots(1, 4, figsize=(16, 4))\n",
    "axs[0].imshow(im)\n",
    "axs[0].set_title(\"Original\")\n",
    "\n",
    "axs[1].imshow(clahe_in_mem)\n",
    "axs[1].set_title(\"CLAHE\")\n",
    "\n",
    "axs[2].imshow(clahe_dask_blockwise)\n",
    "axs[2].set_title(\"CLAHE Dask blockwise\")\n",
    "\n",
    "axs[3].imshow(clahe_dask_blockwise_overlap)\n",
    "axs[3].set_title(\"CLAHE Dask blockwise with overlap\")\n",
    "\n",
    "for a in axs:\n",
    "    a.set_axis_off()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4a7adce-9525-4339-b192-6aecbab11512",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "7f65ce74-4922-4203-a82b-b7e98fbdb825",
   "metadata": {},
   "source": [
    "## 3. [Dask-Image](https://image.dask.org/en/latest/)\n",
    "---\n",
    "A library for n-dimensional image analysis based on Dask Arrays.\n",
    "\n",
    "Features:\n",
    "\n",
    "* Focuses on Dask Arrays\n",
    "* Support for loading image files\n",
    "* Commonly used N-D filters\n",
    "* Working with N-D label images\n",
    "* Common N-D morphological operators\n",
    "\n",
    "GPU support using [CuPy](https://cupy.dev/)!\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc82ffed-3c58-4a72-a842-9dcb07803802",
   "metadata": {},
   "source": [
    "## 4. Example Dask-Image on [BigStitcher](https://imagej.net/plugins/bigstitcher/) registration fused into zarr\n",
    "---\n",
    "\n",
    "### 4.1 Load data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4bc4afa5-5387-4adc-bf20-371062609eec",
   "metadata": {},
   "outputs": [],
   "source": [
    "import stackview  # for viewing\n",
    "from dask_image.ndfilters import gaussian"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c62366b0-3a87-4640-ace1-1cbb7eaf3413",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Any other 5D BigStitcher zarr result will work\n",
    "root_dir = \"T:/IOF-ExchangeDATA/danzlgrp/Nathalie/MERFISH_data_Chris/20240418_ExpID142_Zhang_pool_3D-MERFISH_EtOH/fused/20240418_ExpID142_Zhang_pool_3D-MERFISH_EtOH.zarr\"\n",
    "\n",
    "list_of_frames = [da.from_zarr(f\"{root_dir}/fused_tp_{t}_ch_2s0/\") for t in range(11)]\n",
    "\n",
    "arr = da.stack(list_of_frames).astype(np.float32)\n",
    "arr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "266f1112-788a-456a-bc2c-c0c500b8747f",
   "metadata": {},
   "outputs": [],
   "source": [
    "stackview.slice(arr[0, :, :512, :1024], display_min=100, display_max=500)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "764cb09c-0920-49b8-8782-343193b709be",
   "metadata": {},
   "source": [
    "### 4.2 Difference of Gaussian (DoG)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24cc6f44-381a-4e4a-8fd9-dd6e4d76d443",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr_dog = (gaussian(arr, sigma=(0, 0, 1, 1)) - gaussian(arr, sigma=(0, 0, 2, 2))).clip(\n",
    "    0, None\n",
    ")\n",
    "arr_dog"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e760240a-ed07-4f39-9c62-69c815706005",
   "metadata": {},
   "outputs": [],
   "source": [
    "stackview.slice(arr_dog[0, :, :512, :1024], display_max=64)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b50d987-155d-41d4-9ee3-70bbb2c3525c",
   "metadata": {},
   "source": [
    "### 4.3 Threshold DoG"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f6e80954-7624-4a87-a3f0-26d09738d7ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr_thres = (arr_dog > 64).astype(np.uint8) * 255\n",
    "arr_thres"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9f1ce618-8376-4977-9f81-45cafdd3c865",
   "metadata": {},
   "outputs": [],
   "source": [
    "stackview.slice(arr_thres[0, ..., :512, :1024])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff49f079-7ae3-4d84-b84b-ea04012981a7",
   "metadata": {},
   "source": [
    "### 4.4 Saving will create results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72517cf7-9463-4111-a63b-ee02aab68260",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr_seg = arr_thres[0, :, 1024:2048, 1024:2048]\n",
    "arr_seg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12d3e897-a801-4618-876e-7926889ecfd7",
   "metadata": {},
   "outputs": [],
   "source": [
    "arr_seg.to_zarr(\"my_bead_seg.zarr\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d20b720-60e8-4fd7-8e23-72d1e4116461",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "6b019a75-312d-4949-9ae0-b23de178e330",
   "metadata": {},
   "source": [
    "## Considerations and Limitations\n",
    "\n",
    "* Even with parallelizaton, processing is typically a bit slower than in-memory computaton, due to task graph overhead and need for overlap.\n",
    "* How to choose good [chunks sizes](https://docs.dask.org/en/latest/array-best-practices.html#)?\n",
    "* Dask arrays do not support *fancy indexing*! (only 1D)\n",
    "* No caching so far\n",
    "* Dask-image: No (proper) regionprops, yet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fdd4a8a8-3fb4-4054-965c-2b4aec5dc42d",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33b1a006-209e-4c0d-ba18-19c5bc97e924",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
